<?php

namespace App\Form;

use App\Entity\Figure;
use App\Form\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class FigureType extends AbstractType
{

    /**
    *Permet d'avoir la configuration de base d'un champ
    *
    *@param string $label
    *@param string $placeholder
    *@return array
    */

    private function getConfiguration($label, $placeholder) {
        return [
                'label' => $label,
                'attr'  => [
                            'placeholder' => $placeholder
                ]
            ];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',TextType::class,           $this->getConfiguration("Nom", " Entrez le nom de votre figure"))
            ->add('slug',TextType::class,            $this->getConfiguration("Adresse web", " Entrez l'adresse web de la figure (automatique)"))
            ->add('coverImage',UrlType::class,      $this->getConfiguration("URL de l'image", " Ajouter une image"))
            ->add('introduction',TextType::class,    $this->getConfiguration("Introduction", " Introduction sur la figure"))
            ->add('content',TextareaType::class,     $this->getConfiguration("Contenu", " Description complète de la figure"))
            ->add('category',IntegerType::class,      $this->getConfiguration("Catégorie de la figure", " Ajouter sa catégorie"))
            ->add(
                'images',
                CollectionType::class,
                [
                    'entry_type' => ImageType::class,
                    'allow_add' => true
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Figure::class,
        ]);
    }
}
