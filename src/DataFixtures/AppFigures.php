<?php

namespace App\DataFixtures;

use App\Entity\Figure;
use Faker\Factory;
use App\Entity\Image;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFigures extends Fixture
{
    public function load(ObjectManager $manager)
    {
    	$faker = Factory::create('fr_FR');
    	

    	for($i = 1; $i <= 30; $i++) {
			$figure = new Figure();

		$title 		  = $faker->sentence();
		$coverImage   = $faker->imageUrl(1000,350);
		$introduction = $faker->paragraph(2);
		$content      = '<p>' . join('</p><p>', $faker->paragraphs(5)) . '</p>';
		$createAt = $faker->dateTimeBetween('-6 months');

    	$figure->setTitle($title)
    		   ->setCoverImage($coverImage)
    		   ->setIntroduction($introduction)
    		   ->setContent($content)
    		   ->setCategory(mt_rand(1,3))
    		   ->setCreateAt($createAt);

    	for($j = 1; $j <= mt_rand(2,5); $j++) {
    		$image = new Image();

    		$image->setUrl($faker->imageUrl())
    			  ->setCaption($faker->sentence())
    			  ->setFigure($figure);

    		$manager->persist($image);	  

    	}
       
    	$manager->persist($figure);
    }

        $manager->flush();
    }
}
