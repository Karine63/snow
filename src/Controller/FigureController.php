<?php

namespace App\Controller;

use App\Entity\Figure;
use App\Entity\Image;
use App\Form\FigureType;
use App\Repository\FigureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

class FigureController extends AbstractController
{
    /**
     * @Route("/figures", name="figures_index")
     */
    public function index(FigureRepository $repo)
    {
    	
    	$figures = $repo->findAll();

        return $this->render('figure/index.html.twig', [
           'figures' => $figures
        ]);
    }

 /**
    * create a figure
    *
    * @Route("/figures/new", name="figures_create")
    *
    * @return Response
    */
    public function create(Request $request, ObjectManager $manager){
    	$figure = new Figure();

    	$form = $this->createForm(FigureType::class, $figure);

    	$form->handleRequest($request);


    	if($form->isSubmitted() && $form->isValid()){
    		$manager->persist($figure);
    		$manager->flush();

    		$this->addFlash(
    			'succes',
    			"La figure <strong>{$figure->getTitle()}</strong> a bien été enregistrée !"
    		);

    		return $this->redirectToRoute('figures_show', [
    				'slug' => $figure->getSlug()
    		]);
    	}

    	return $this->render('figure/new.html.twig', [
    			'form' => $form->createView()
    	]);
    }


    /**
    * displays a single figure
    *
    * @Route("/figures/{slug}", name="figures_show")
    *
    * @return Response
    */
    public function show(Figure $figure){
    	return $this->render('figure/show.html.twig', [
    		'figure' => $figure
    	]);
    }
}

